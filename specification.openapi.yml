openapi: 3.0.0
info:
  title: Hackaton - Ranked Votes API REST Documentation
  description: Voting for Hackaton
  version: 1.0.0
  license:
    name: GPL 3.0+
    url: https://www.gnu.org/licenses/gpl-3.0.en.html

servers:
  - url: http://localhost:8080
    description: development local server

tags:
  - name: Service
    description: service status
  - name: Session
    description: session management
  - name: Users
    description: admin users management
  - name: Posts
    description: posts management
  - name: Elections
    description: elections management
  - name: Votes
    description: votes management

components:
  securitySchemes:
    BearerAuth:
      type: http
      scheme: bearer

paths:
  /:
    get:
      description: service version and status
      tags: [Service]
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: string
              example: Hackaton Ranking API v1.0.0
  /login:
    post:
      summary: get admin user session token
      tags: [Session]
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                password:
                  type: string
                token:
                  type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  session:
                    type: string
                  user_id:
                    type: number
                    format: int32
  /logout:
    delete:
      summary: remove admin user session token
      tags: [Session]
      security:
        - BearerAuth: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  session:
                    type: string
  /users:
    post:
      summary: Create user
      tags: [Users]
      security:
        - BearerAuth: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required:
                - email
              properties:
                email:
                  type: string
                password:
                  type: string
                nickname:
                  type: string
                admin:
                  type: boolean
      responses:
        '200':
          description: user created
          content:
            application/json:
              schema:
                type: object
              example:
                id: 24
                nickname: test q
                email: testq@mail.com
                admin: false
                created_at: '2024-04-28T19:03:24.938340482-03:00'
                updated_at: '2024-04-28T19:03:24.938340533-03:00'
    get:
      summary: Get all users
      tags: [Users]
      security:
        - BearerAuth: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
              example:
                - id: 1
                  nickname: Steven victor
                  email: steven@gmail.com
                  admin: true
                  created_at: '2024-04-25T23:49:12.77678243-03:00'
                  updated_at: '2024-04-25T23:49:12.77678243-03:00'
                - id: 2
                  nickname: Martin Luther
                  email: luther@gmail.com
                  admin: false
                  created_at: '2024-04-25T23:49:12.849051171-03:00'
                  updated_at: '2024-04-25T23:49:12.849051171-03:00'
                - id: 18
                  nickname: test
                  email: test@mail.com
                  admin: false
                  created_at: '2024-04-28T18:50:37.85772778-03:00'
                  updated_at: '2024-04-28T18:50:37.85772787-03:00'
                - id: 19
                  nickname: test uno
                  email: test1@mail.com
                  created_at: '2024-04-28T18:51:12.894190581-03:00'
                  updated_at: '2024-04-28T18:51:12.894190641-03:00'
                - id: 24
                  nickname: test q
                  email: testq@mail.com
                  admin: false
                  created_at: '2024-04-28T19:03:24.938340482-03:00'
                  updated_at: '2024-04-28T19:03:24.938340533-03:00'
  /users/{id}:
    get:
      summary: Get User by ID
      tags: [Users]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 1
                nickname: Steven victor
                email: steven@gmail.com
                admin: false
                created_at: '2024-04-25T23:49:12.77678243-03:00'
                updated_at: '2024-04-25T23:49:12.77678243-03:00'
    put:
      summary: Modify user by ID
      tags: [Users]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                password:
                  type: string
                nickname:
                  type: string
                admin:
                  type: boolean
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 1
                nickname: Steven victor
                email: steven@gmail.com
                admin: false
                created_at: '2024-04-25T23:49:12.77678243-03:00'
                updated_at: '2024-04-25T23:49:12.77678243-03:00'
    delete:
      summary: Delete user by ID
      tags: [Users]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '204':
          description: OK
  /users/{id}/password:
    put:
      summary: Reset user password by ID
      tags: [Users]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 1
                nickname: Steven victor
                email: steven@gmail.com
                admin: false
                created_at: '2024-04-25T23:49:12.77678243-03:00'
                updated_at: '2024-04-25T23:49:12.77678243-03:00'
  /posts:
    post:
      summary: Create post
      tags: [Posts]
      security:
        - BearerAuth: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required:
                - title
                - content
                - author_id
              properties:
                title:
                  type: string
                content:
                  type: string
                author_id:
                  type: number
      responses:
        '200':
          description: post created
          content:
            application/json:
              schema:
                type: object
              example:
                id: 41
                title: test post
                content: this is a test post...
                author:
                  id: 44
                  nickname: Steven victor
                  email: steven@gmail.com
                  admin: true
                  created_at: '2024-04-28T23:50:14Z'
                  updated_at: '2024-04-28T23:50:14Z'
                author_id: 44
                created_at: '2024-04-28T21:17:56.995467052-03:00'
                updated_at: '2024-04-28T21:17:56.995467143-03:00'
    get:
      summary: Get all posts
      tags: [Posts]
      security:
        - BearerAuth: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
              example:
                - id: 1
                  title: Title 1
                  content: Hello world 1
                  author:
                    id: 1
                    nickname: t uno dos tres
                    email: t123@mail.com
                    admin: true
                    created_at: '2024-04-25T23:49:12.77678243-03:00'
                    updated_at: '2024-04-28T20:50:33.049779356-03:00'
                  author_id: 1
                  created_at: '2024-04-25T23:49:12.78677858-03:00'
                  updated_at: '2024-04-25T23:49:12.78677858-03:00'
                - id: 2
                  title: Title 2
                  content: Hello world 2
                  author:
                    id: 2
                    nickname: Martin Luther
                    email: luther@gmail.com
                    admin: true
                    created_at: '2024-04-25T23:49:12.849051171-03:00'
                    updated_at: '2024-04-25T23:49:12.849051171-03:00'
                  author_id: 2
                  created_at: '2024-04-25T23:49:12.858846869-03:00'
                  updated_at: '2024-04-25T23:49:12.858846869-03:00'
  /posts/{id}:
    get:
      summary: Get post by ID
      tags: [Posts]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 1
                title: Title 1
                content: Hello world 1
                author:
                  id: 1
                  nickname: t uno dos tres
                  email: t123@mail.com
                  admin: true
                  created_at: '2024-04-25T23:49:12.77678243-03:00'
                  updated_at: '2024-04-28T20:50:33.049779356-03:00'
                author_id: 1
                created_at: '2024-04-25T23:49:12.78677858-03:00'
                updated_at: '2024-04-25T23:49:12.78677858-03:00'
    put:
      summary: Modify post by ID
      tags: [Posts]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required:
                - title
                - content
                - author_id
              properties:
                title:
                  type: string
                content:
                  type: string
                author_id:
                  type: number
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 41
                title: test post
                content: this is a test post. Testing ... testing.
                author:
                  id: 44
                  nickname: Steven victor
                  email: steven@gmail.com
                  admin: true
                  created_at: '2024-04-28T23:50:14Z'
                  updated_at: '2024-04-28T23:50:14Z'
                author_id: 44
                created_at: '2024-04-28T21:22:17.225163595-03:00'
                updated_at: '2024-04-28T21:22:17.225163686-03:00'
    delete:
      summary: Delete post by ID
      tags: [Posts]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '204':
          description: OK
  /elections:
    post:
      summary: Create election
      tags: [Elections]
      security:
        - BearerAuth: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required:
                - title
                - content
                - author_id
              properties:
                title:
                  type: string
                description:
                  type: string
                author_id:
                  type: number
                candidates:
                  type: array
                  items:
                    type: object
                    properties:
                      title:
                        type: string
                      description:
                        type: string
                data:
                  type: object
      responses:
        '200':
          description: election created
          content:
            application/json:
              schema:
                type: object
              example:
                id: 3
                title: Title BlaBla
                description: Bla bla bla blña
                author:
                  id: 1
                  nickname: Steven victor
                  email: steven@gmail.com
                  admin: true
                  created_at: '2024-05-04T15:16:15Z'
                  updated_at: '2024-05-04T15:16:15Z'
                author_id: 1
                candidates:
                  - description: first candidate 1
                    title: candidate 1
                  - description: second candidate 2
                    title: candidate 2
                  - description: third candidate 3
                    title: candidate 3
                  - description: forth candidate 4
                    title: candidate 4
                data:
                  end: '2024-05-05T20:11:59Z'
                  start: '2024-05-05T15:11:59Z'
                created_at: '2024-05-04T12:19:47.278987016-03:00'
                updated_at: '2024-05-04T12:19:47.278987196-03:00'
    get:
      summary: Get all elections
      tags: [Elections]
      security:
        - BearerAuth: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
              example:
                - id: 1
                  title: Title 1
                  description: Hello world 1
                  author:
                    id: 1
                    nickname: Steven victor
                    email: steven@gmail.com
                    admin: true
                    created_at: '2024-05-04T15:16:15Z'
                    updated_at: '2024-05-04T15:16:15Z'
                  author_id: 1
                  candidates:
                    - description: first candidate
                      title: candidate A
                    - description: second candidate
                      title: candidate B
                    - description: third candidate
                      title: candidate C
                    - description: forth candidate
                      title: candidate D
                  data: null
                  created_at: '2024-05-04T15:16:15Z'
                  updated_at: '2024-05-04T15:16:15Z'
                - id: 2
                  title: Title 2
                  description: Hello world 2
                  author:
                    id: 1
                    nickname: Steven victor
                    email: steven@gmail.com
                    admin: true
                    created_at: '2024-05-04T15:16:15Z'
                    updated_at: '2024-05-04T15:16:15Z'
                  author_id: 1
                  candidates:
                    - description: first candidate
                      title: candidate 1
                    - description: second candidate
                      title: candidate 2
                    - description: third candidate
                      title: candidate 3
                    - description: forth candidate
                      title: candidate 4
                  data:
                    end: '2024-05-04T20:11:59Z'
                    start: '2024-05-04T15:11:59Z'
                  created_at: '2024-05-04T15:16:15Z'
                  updated_at: '2024-05-04T15:16:15Z'
  /elections/{id}:
    get:
      summary: Get elections by ID
      tags: [Elections]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 1
                title: Title 1
                description: Hello world 1
                author:
                  id: 1
                  nickname: Steven victor
                  email: steven@gmail.com
                  admin: true
                  created_at: '2024-05-04T15:16:15Z'
                  updated_at: '2024-05-04T15:16:15Z'
                author_id: 1
                candidates:
                  - description: first candidate
                    title: candidate A
                  - description: second candidate
                    title: candidate B
                  - description: third candidate
                    title: candidate C
                  - description: forth candidate
                    title: candidate D
                data: null
                created_at: '2024-05-04T15:16:15Z'
                updated_at: '2024-05-04T15:16:15Z'
    put:
      summary: Modify election by ID
      tags: [Elections]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required:
                - title
                - content
                - author_id
              properties:
                title:
                  type: string
                description:
                  type: string
                author_id:
                  type: number
                candidates:
                  type: array
                  items:
                    type: object
                    properties:
                      title:
                        type: string
                      description:
                        type: string
                data:
                  type: object
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 3
                title: Title BlaBla 1
                description: Bla bla bla blña
                author:
                  id: 1
                  nickname: Steven victor
                  email: steven@gmail.com
                  admin: true
                  created_at: '2024-05-04T15:16:15Z'
                  updated_at: '2024-05-04T15:16:15Z'
                author_id: 1
                candidates:
                  - description: first candidate 1
                    title: candidate 1
                  - description: second candidate 2
                    title: candidate 2
                  - description: third candidate 3
                    title: candidate 3
                  - description: forth candidate 4
                    title: candidate 4
                data:
                  end: '2024-05-05T20:11:59Z'
                  start: '2024-05-05T15:11:59Z'
                created_at: '2024-05-04T12:25:46.608337894-03:00'
                updated_at: '2024-05-04T12:25:46.608337995-03:00'
    delete:
      summary: Delete election by ID
      tags: [Elections]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '204':
          description: OK
  /elections/{id}/result:
    get:
      summary: Get election result by ID
      tags: [Elections]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
              example:
                id: 1
                title: Title 1
                description: Hello world 1
                author:
                  id: 1
                  nickname: Steven victor
                  email: steven@gmail.com
                  admin: true
                  created_at: '2024-05-04T15:16:15Z'
                  updated_at: '2024-05-04T15:16:15Z'
                author_id: 1
                candidates:
                  - description: first candidate
                    title: candidate A
                  - description: second candidate
                    title: candidate B
                  - description: third candidate
                    title: candidate C
                  - description: forth candidate
                    title: candidate D
                data: null
                created_at: '2024-05-04T15:16:15Z'
                updated_at: '2024-05-04T15:16:15Z'
  /votes:
    post:
      summary: Create vote
      tags: [Votes]
      security:
        - BearerAuth: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              required:
                - election_id
                - author_id
                - ranking
              properties:
                election_id:
                  type: number
                author_id:
                  type: number
                ranking:
                  type: array
                  items:
                    type: object
                    properties:
                      title:
                        type: string
                      description:
                        type: string
      responses:
        '200':
          description: vote created
          content:
            application/json:
              schema:
                type: object
    get:
      summary: Get all votes
      tags: [Votes]
      security:
        - BearerAuth: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
  /votes/{id}:
    get:
      summary: Get votes by ID
      tags: [Votes]
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            format: int32
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
