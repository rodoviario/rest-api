package controllers

import (
	"net/http"

	m "gitlab.com/hackaton4.0Ranking/rest-api/api/middlewares"
)

func (s *Server) initializeRoutes() {

	// Home Route
	s.Router.HandleFunc("/", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.Home))).Methods(http.MethodGet, http.MethodOptions)

	// Login Route
	s.Router.HandleFunc("/login", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.Login))).Methods(http.MethodPost, http.MethodOptions)
	s.Router.HandleFunc("/logout", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.Logout))).Methods(http.MethodDelete, http.MethodOptions)

	//Users routes
	s.Router.HandleFunc("/users", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.CreateUser)))).Methods(http.MethodPost, http.MethodOptions)
	s.Router.HandleFunc("/users", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.GetUsers)))).Methods(http.MethodGet, http.MethodOptions)
	s.Router.HandleFunc("/users/{id}", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthVoter(s.GetUser)))).Methods(http.MethodGet, http.MethodOptions)
	s.Router.HandleFunc("/users/{id}", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.UpdateUser)))).Methods(http.MethodPut, http.MethodOptions)
	s.Router.HandleFunc("/users/{id}/password", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.ResetUserPassword)))).Methods(http.MethodPut, http.MethodOptions)
	s.Router.HandleFunc("/users/{id}", m.SetMiddlewareCORS(m.SetMiddlewareAuthAdmin(s.DeleteUser))).Methods(http.MethodDelete, http.MethodOptions)

	//Posts routes
	s.Router.HandleFunc("/posts", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.CreatePost)))).Methods(http.MethodPost, http.MethodOptions)
	s.Router.HandleFunc("/posts", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.GetPosts))).Methods(http.MethodGet, http.MethodOptions)
	s.Router.HandleFunc("/posts/{id}", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.GetPost))).Methods(http.MethodGet, http.MethodOptions)
	s.Router.HandleFunc("/posts/{id}", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.UpdatePost)))).Methods(http.MethodPut, http.MethodOptions)
	s.Router.HandleFunc("/posts/{id}", m.SetMiddlewareCORS(m.SetMiddlewareAuthAdmin(s.DeletePost))).Methods(http.MethodDelete, http.MethodOptions)

	//Election routes
	s.Router.HandleFunc("/elections", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.CreateElection)))).Methods(http.MethodPost, http.MethodOptions)
	s.Router.HandleFunc("/elections", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.GetElections))).Methods(http.MethodGet, http.MethodOptions)
	s.Router.HandleFunc("/elections/{id}", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.GetElection))).Methods(http.MethodGet, http.MethodOptions)
	s.Router.HandleFunc("/elections/{id}", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.UpdateElection)))).Methods(http.MethodPut, http.MethodOptions)
	s.Router.HandleFunc("/elections/{id}", m.SetMiddlewareCORS(m.SetMiddlewareAuthAdmin(s.DeleteElection))).Methods(http.MethodDelete, http.MethodOptions)
	s.Router.HandleFunc("/elections/{id}/result", m.SetMiddlewareCORS(m.SetMiddlewareJSON(s.GetElectionResult))).Methods(http.MethodGet, http.MethodOptions)

	//Vote routes
	s.Router.HandleFunc("/votes", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthNotAdmin(s.CreateVote)))).Methods(http.MethodPost, http.MethodOptions)
	s.Router.HandleFunc("/votes", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.GetVotes)))).Methods(http.MethodGet, http.MethodOptions)
	s.Router.HandleFunc("/votes/{id}", m.SetMiddlewareCORS(m.SetMiddlewareJSON(m.SetMiddlewareAuthAdmin(s.GetVote)))).Methods(http.MethodGet, http.MethodOptions)
}
