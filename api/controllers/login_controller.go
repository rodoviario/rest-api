package controllers

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/hackaton4.0Ranking/rest-api/api/auth"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/models"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/responses"
	"gitlab.com/hackaton4.0Ranking/rest-api/api/utils/formaterror"
	"golang.org/x/crypto/bcrypt"
)

func (server *Server) Login(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	user := models.LoginUser{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user.Prepare()
	err = user.Validate("login")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	if user.Token != "" {
		email, password, err := auth.ExtractLoginToken(user.Token)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}
		user.Email = email
		user.Password = password
	}
	uid, token, err := server.SignIn(user.Email, user.Password)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusUnprocessableEntity, formattedError)
		return
	}
	responses.JSON(w, http.StatusOK, map[string]interface{}{ "session": token, "user_id": uid } )
}

func (server *Server) Logout(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, map[string]string{ "status": "OK" } )
}

func (server *Server) SignIn(email, password string) (uint32, string, error) {

	var err error

	user := models.User{}

	err = server.DB.Debug().Model(models.User{}).Where("email = ?", email).Take(&user).Error
	if err != nil {
		return 0, "", err
	}
	err = models.VerifyPassword(user.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return 0, "", err
	}
	token, err := auth.CreateToken(user.ID, user.Admin)
	if !user.Admin {
		// single use password
		user.ResetUserPassword(server.DB, user.ID, false)
	}
	return user.ID, token, err
}
