package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"gorm.io/gorm"
)

type Election struct {
	ID          uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Title       string    `gorm:"size:255;not null;unique" json:"title"`
	Description string    `gorm:"size:255;not null;" json:"description"`
	Author      User      `json:"author"`
	AuthorID    uint32    `sql:"type:int REFERENCES users(id)" json:"author_id"`
	Candidates  JsonArray `json:"candidates"`
	Data        JSON      `json:"data"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (p *Election) Prepare() {
	p.ID = 0
	p.Title = html.EscapeString(strings.TrimSpace(p.Title))
	p.Description = html.EscapeString(strings.TrimSpace(p.Description))
	p.Author = User{}
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()
}

func (p *Election) Validate() error {

	if p.Title == "" {
		return errors.New("required Title")
	}
	if p.Description == "" {
		return errors.New("required Description")
	}
	if p.AuthorID < 1 {
		return errors.New("required Author")
	}
	if len(p.Candidates) < 1 {
		return errors.New("required Candidates")
	}
	for i, c1 := range p.Candidates {
		for j, c2 := range p.Candidates {
			if c1["title"] == c2["title"] && i != j {
				return errors.New("required Candidates with unique title")
			}
		}
	}
	return nil
}

func (p *Election) SaveElection(db *gorm.DB) (*Election, error) {
	var err error
	err = db.Debug().Model(&Election{}).Create(&p).Error
	if err != nil {
		return &Election{}, err
	}
	if p.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", p.AuthorID).Omit("password").Take(&p.Author).Error
		if err != nil {
			return &Election{}, err
		}
	}
	return p, nil
}

func (p *Election) FindAllElections(db *gorm.DB) (*[]Election, error) {
	var err error
	elections := []Election{}
	err = db.Debug().Model(&Election{}).Limit(100).Find(&elections).Error
	if err != nil {
		return &[]Election{}, err
	}
	if len(elections) > 0 {
		for i := range elections {
			err := db.Debug().Model(&User{}).Where("id = ?", elections[i].AuthorID).Omit("password").Take(&elections[i].Author).Error
			if err != nil {
				return &[]Election{}, err
			}
		}
	}
	return &elections, nil
}

func (p *Election) FindElectionByID(db *gorm.DB, pid uint64) (*Election, error) {
	var err error
	err = db.Debug().Model(&Election{}).Where("id = ?", pid).Take(&p).Error
	if err != nil {
		return &Election{}, err
	}
	if p.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", p.AuthorID).Omit("password").Take(&p.Author).Error
		if err != nil {
			return &Election{}, err
		}
	}
	return p, nil
}

func (p *Election) UpdateAElection(db *gorm.DB) (*Election, error) {

	var err error
	err = db.Debug().Model(&Election{}).Where("id = ?", p.ID).Updates(Election{Title: p.Title, Description: p.Description, Candidates: p.Candidates, Data: p.Data, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Election{}, err
	}
	if p.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", p.AuthorID).Omit("password").Take(&p.Author).Error
		if err != nil {
			return &Election{}, err
		}
	}
	return p, nil
}

func (p *Election) DeleteAElection(db *gorm.DB, pid uint64, uid uint32) (int64, error) {

	db = db.Debug().Model(&Election{}).Where("id = ? and author_id = ?", pid, uid).Take(&Election{}).Delete(&Election{})

	if db.Error != nil {
		if errors.Is(db.Error, gorm.ErrRecordNotFound) {
			return 0, errors.New("Election not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
